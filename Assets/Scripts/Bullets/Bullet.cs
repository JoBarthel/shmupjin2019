using UnityEngine;

/**
 * Mother class for all projectiles
 */
public abstract class Bullet : MonoBehaviour, Pool<Bullet>.IPoolable
{
    /* Who this bullet can hurt */
    private BulletFaction bulletFaction;
    [SerializeField] private BulletBehaviour bulletBehaviour;
    private float damage;
    
    /* callback to call on destruction to be recycled via the pooling system */
    private Pool<Bullet>.ReturnToPool returnToPool;
    
    protected Vector2 BaseSpeed { get; private set; }

    public virtual BulletBehaviour BulletBehaviour => bulletBehaviour;

    public virtual void Init(Vector2 speed, float damage, BulletFaction bulletFaction, Vector2 startingDirection, Vector2 startingPosition)
    {
        BaseSpeed = speed;
        this.damage = damage;
        this.bulletFaction = bulletFaction;
        transform.position = startingPosition;
        transform.right = startingDirection;
    }
    
    private void OnBecameInvisible()
    {
        Die();
    }

    private void Die()
    {
        if(returnToPool != null)
            returnToPool(this);
        else 
            Destroy(this);
    }

    public void SetReturnCallback(Pool<Bullet>.ReturnToPool callback)
    {
        returnToPool = callback;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        // check if collision is valid todo: make the check prettier
        if (bulletFaction == BulletFaction.Enemy && other.CompareTag("Enemy")) return;
        if (bulletFaction == BulletFaction.Player && other.CompareTag("Player")) return;
        var damageable = other.GetComponent<IDamageable>();
        damageable?.SufferDamage(damage);
        Die();
    }
}

