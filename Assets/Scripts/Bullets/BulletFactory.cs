using System.Collections.Generic;
using UnityEngine;

/**
 * Singleton that handles the bullet instantiation
 */
public class BulletFactory : MonoBehaviour
{
    public static BulletFactory Instance { get; private set; }
    [SerializeField] private Bullet[] availableBulletPrefabs;
    
    // they are stored in a dictionary at start-up
    private static Dictionary<BulletBehaviour, Pool<Bullet>> bulletTypes;

    private void Awake()
    {
        // Singleton boilerplate code
        Debug.Assert(Instance == null);
        Instance = this;
        
        // Build our dictionary of bullet types
        bulletTypes = new Dictionary<BulletBehaviour, Pool<Bullet>>();
        foreach (var bullet in availableBulletPrefabs)
        {
            if (!bulletTypes.ContainsKey(bullet.BulletBehaviour))
                bulletTypes.Add(bullet.BulletBehaviour, new Pool<Bullet>(bullet, transform));
        }
    }
    
    /*
     * Return a bullet with the correct BulletBehaviour from a pool
     */
    public Bullet Get(BulletBehaviour lineBehaviour)
    {
        try
        {
            if (bulletTypes.ContainsKey(lineBehaviour)) return bulletTypes[lineBehaviour].Get();
        }
        catch(InstanceWasDestroyedException e)
        {
            RichLog.Warning(e.Message, typeof(BulletFactory));
        }

        throw new RequestUnknownTypeFromFactoryException();
    }

}