using UnityEngine;

public class SimpleBullet : Bullet
{
    private void Update()
    {
        transform.Translate(BaseSpeed * transform.right * Time.deltaTime);
    }
}