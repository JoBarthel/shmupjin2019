﻿using UnityEngine;

public class SpiralBullet : Bullet
{
    /// <summary>
    /// Set the distance between successive turnings.
    /// </summary>
    [SerializeField]
    private float distanceBetweenSuccessiveTurnings;

    /// <summary>
    /// Pulsation in radian per seconds.
    /// </summary>
    private float pulsation;

    private float startTime;
    private Vector2 startPosition;

    private float startAngleCosinus;
    private float startAngleSinus;
  
    public override BulletBehaviour BulletBehaviour => BulletBehaviour.Spiral;
    public override void Init(Vector2 speed, float damage, BulletFaction bulletFaction, Vector2 startingDirection, Vector2 startingPosition)
    {
        base.Init(speed, damage, bulletFaction, startingDirection, startingPosition);

        // Compute the start angle parameters.
        float startAngle = Mathf.Atan2(startingDirection.y, startingDirection.x);
        this.startAngleCosinus = Mathf.Cos(startAngle);
        this.startAngleSinus = Mathf.Sin(startAngle);

        // Set the pulsation.
        pulsation = speed.magnitude;

        startPosition = transform.position;
        startTime = Time.time;
    }

    protected void Update()
    {
        // Compute the spiral position.
        float t = Time.time - startTime;
        float theta = pulsation * t;

        float dx = distanceBetweenSuccessiveTurnings * theta * Mathf.Cos(theta);
        float dy = distanceBetweenSuccessiveTurnings * theta * Mathf.Sin(theta);
        var spiralPosition = new Vector2(dx, dy);

        // Rotation and offset.
        float x = (spiralPosition.x * startAngleCosinus) - (spiralPosition.y * startAngleSinus);
        float y = (spiralPosition.x * startAngleSinus) + (spiralPosition.y * startAngleCosinus);
        transform.position = startPosition + new Vector2(x, y);
    }
}