public interface IDamageable
{
    void SufferDamage(float damage);
}