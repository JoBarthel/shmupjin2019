﻿using UnityEngine;

public class AiEnemyAdvancedEngine : AiEnemyBasicEngine
{ 
    protected override Vector2 Move(Vector2 direction)
    {
        return new Vector2(direction.x, Mathf.Cos(Time.time)) ;
    }
}
