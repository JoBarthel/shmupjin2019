using System;
using UnityEngine;
using Random = System.Random;

public class AiEnemyBasicEngine : MonoBehaviour {
    private Engine engine;
    private BulletGun bulletGun;

    [SerializeField] private float minSpeed;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float minCooldown;
    [SerializeField] private float maxCooldown;

    private void Awake()
    {
        engine = GetComponent<Engine>();
        if (!engine) Debug.LogWarning($"Could not find an Engine in object in {GetType()} {name}");
        else engine.Speed = UnityEngine.Random.Range(minSpeed, maxSpeed);
        
        bulletGun = GetComponent<BulletGun>();
        if (!bulletGun) Debug.LogWarning($"Could not find a BulletGun in object in {GetType()} {name}");
        bulletGun.Cooldown = UnityEngine.Random.Range(minCooldown, maxCooldown);
    }

    protected virtual Vector2 Move(Vector2 direction)
    {
        return direction;
    }
    
    private void Update()
    {
        engine.Move(Move(Vector2.right));
        bulletGun.Fire();
    }

}