using UnityEngine;

/**
 * Handle an enemy health and pooling
 */
public class EnemyAvatar : BaseAvatar, Pool<EnemyAvatar>.IPoolable
{
    private Pool<EnemyAvatar>.ReturnToPool returnToPoolCallback;
    
    /* we need the camera's rect to detect when we go out of bounds */
    private Vector2 screenBounds;
    /* we can go this far out of bounds before being destroyed */
    [SerializeField] private float margin = 5;
    
    [SerializeField] private EnemyType type;
    public EnemyType Type => type;

    protected override void Awake()
    {
        base.Awake();
        pickupSpawner = new PickupSpawner();
        screenBounds = Camera.main.ScreenToWorldPoint( new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    private void Update()
    {
        if(IsOutOfBounds()) Die();
    }

    private bool IsOutOfBounds()
    {
        if (transform.position.x > screenBounds.x + margin || 
            transform.position.x < -screenBounds.x - margin || 
            transform.position.y > screenBounds.y + margin ||
            transform.position.y < -screenBounds.y - margin)
            return true;
        return false;
    }

    public override void SufferDamage(float damage)
    {
        health -= damage;
        if(health <= 0)
            Die();
    }

    private PickupSpawner pickupSpawner;
    [SerializeField] private float pickUpSpawnChanceOutOfOne;
    private void Die()
    {
        NotifyDeath();
        if(Random.Range(0f, 1f) < pickUpSpawnChanceOutOfOne)
            pickupSpawner?.SpawnRandomPickup(transform.position);
        returnToPoolCallback(this);
    }
    
    [SerializeField] private int scoreWorthOnDeath;
    public delegate void OnDeath(int i);
    public event OnDeath OnDeathEvent;
    void NotifyDeath()
    {
        OnDeathEvent.Invoke(scoreWorthOnDeath);
    }
    
    public void SetReturnCallback(Pool<EnemyAvatar>.ReturnToPool callback)
    {
        returnToPoolCallback = callback;
    }
    

}
