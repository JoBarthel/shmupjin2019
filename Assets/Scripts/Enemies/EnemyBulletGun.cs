﻿public class EnemyBulletGun : BulletGun
{
    
    public override void Fire()
    {
        if(timeSpentOnCooldown < cooldown) return;
        timeSpentOnCooldown = 0;
        base.Fire();
    }
}
