﻿using System.Collections.Generic;
using UnityEngine;

/**
 * Singleton that handles the enemy instantiation
 */
public class EnemyFactory : MonoBehaviour
{
    public static EnemyFactory Instance { get; private set; }

    // the different types of enemies are filled in from the editor
    [SerializeField] private EnemyAvatar[] enemyPrefabs;
    
    // they are stored in a dictionary at start-up
    private readonly Dictionary<EnemyType, Pool<EnemyAvatar>> enemyTypes = new Dictionary<EnemyType, Pool<EnemyAvatar>>();
    
    private void Awake()
    {
        // Singleton boilerplate coe
        Debug.Assert(Instance == null);
        Instance = this;

        // build our dictionary of enemy types
        foreach (var enemy in enemyPrefabs)
        {
            if (!enemyTypes.ContainsKey(enemy.Type))
                enemyTypes.Add(enemy.Type, new Pool<EnemyAvatar>(enemy, transform));
        }
    }

    /**
     * Return an enemy of the correct EnemyType from a Pools
     */
    public EnemyAvatar Get(EnemyType type)
    {
        if (enemyTypes.ContainsKey(type))
            return enemyTypes[type].Get();
        
        Debug.LogWarning($"Tried instantiating an enemy type that doesn't exist at {GetType()} {name}.");
        throw new UnknownEnemyTypeException();
    }
}
