﻿using System.Collections.Generic;
using UnityEngine;

/**
 * Handle enemy and player spawning
 * As well as victory conditions
 */
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; set; }

    [SerializeField] private Vector2 playerSpawnPosition;
    [SerializeField] private PlayerAvatar playerPrefab;
    [SerializeField] private UiManager uiManager;

    private void Awake()
    {
        if (Instance != null) Destroy(gameObject);
        Instance = this;
    }

    private PlayerAvatar SpawnPlayer()
    {
        PlayerAvatar player = Instantiate(playerPrefab, playerSpawnPosition, Quaternion.identity);
        Engine engine = player.GetComponent<Engine>();
        if (engine == null) Debug.LogWarning($"Player prefab has no engine component in {GetType()} {name}");
        PlayerBulletGun weapon = player.GetComponent<PlayerBulletGun>();
        if (weapon == null) Debug.LogWarning($"Player prefab has no weapon component in {GetType()} {name}");
        Controls unused = new Controls(engine, weapon);
        return player;
    }

    private void Start()
    {
                
        Debug.Assert(ProgressionManager.Instance != null,
            "There is no ProgressionManager in this scene, levels cannot be loaded");
        
        PlayerAvatar player = SpawnPlayer();
        player.OnPlayerGotHitEvent += OnPlayerGotHit;
        LoadLevel(new LevelLoader().Load(ProgressionManager.Instance.CurrentLevelPath), ProgressionManager.Instance.GameMode);
        uiManager.Init(player, victoryConditionChecker);

    }

    private Queue<EnemyInfo> enemiesToSpawn;
    private VictoryConditionChecker victoryConditionChecker;
    [SerializeField] private ComboParameters comboParameters;
    private void LoadLevel(Level level, GameMode instanceGameMode)
    {
        List<EnemyInfo> enemies = new List<EnemyInfo>();
        foreach (EnemyInfo enemy in level.enemies)
        {
            enemies.Add(enemy);
        }

        // sort enemies by spawn time
        enemies.Sort((enemy, enemy1) => enemy.spawnTime.CompareTo(enemy1.spawnTime));

        // we now have a queue of enemies in spawn order
        enemiesToSpawn = new Queue<EnemyInfo>(enemies);

        if(instanceGameMode == GameMode.Survival)
            victoryConditionChecker = new SurvivalModeVictoryChecker(enemiesToSpawn.Count);
        else
            victoryConditionChecker = new ScoringConditionChecker( enemiesToSpawn.Count, comboParameters);
    }

    private bool enemyCanSpawn = true;

    private void Update()
    {
        if (!enemyCanSpawn) return;

        if (enemiesToSpawn.Count == 0)
        {
            enemyCanSpawn = false;
            return;
        }

        if (Time.time >= enemiesToSpawn.Peek().spawnTime)
            SpawnEnemy(enemiesToSpawn.Dequeue());
    }

    [SerializeField] private Vector2 enemySpawnOrigin;
    [SerializeField] private Vector2 enemyRotationAtStart;

    private void SpawnEnemy(EnemyInfo enemyInfo)
    {
        EnemyAvatar enemyAvatar = EnemyFactory.Instance.Get(enemyInfo.type);
        enemyAvatar.transform.position = enemyInfo.position + enemySpawnOrigin;
        enemyAvatar.transform.right = enemyRotationAtStart;
        enemyAvatar.OnDeathEvent += OnEnemyDied;
    }

    private bool gameIsOver;
    [SerializeField] private LevelTransition transition;
    [SerializeField] private AudioSource audioManager;
    private void OnEnemyDied(int scoreWorthOnDeath)
    {
        if (gameIsOver) return;

        GameState gameState = victoryConditionChecker.EnemyDied(scoreWorthOnDeath);
        OnScoreChangedEvent.Invoke(victoryConditionChecker.GetCurrentScore());
        audioManager.Play();
        if (gameState == GameState.Won)
        {
            ProgressionManager.Instance.LoadNextLevel(victoryConditionChecker.GetCurrentScore(), transition);
            gameIsOver = true;
        }
    }

    private void OnPlayerGotHit(float remainingLife)
    {
        if (gameIsOver) return;
        GameState gameState = victoryConditionChecker.PlayerGotHit(remainingLife);
        OnScoreChangedEvent.Invoke(victoryConditionChecker.GetCurrentScore());
        if (gameState == GameState.Lost)
        {
            ProgressionManager.Instance.EndGame(victoryConditionChecker.GetCurrentScore());
            gameIsOver = true;
        }
    }

    public delegate void OnScoreChanged(int currentScore);

    public event OnScoreChanged OnScoreChangedEvent;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(playerSpawnPosition, 1);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(enemySpawnOrigin, 1);
    }

    public void GainComboBonus(int multiplicator)
    {
        if (victoryConditionChecker is ScoringConditionChecker scoringConditionChecker)
            scoringConditionChecker.GainComboBonus(multiplicator);
    }
}