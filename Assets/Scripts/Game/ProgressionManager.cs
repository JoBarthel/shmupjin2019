using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * A singleton that holds the path to all levels in the game
 * and keep track of which on is currently being played and in which game mode
 */
public class ProgressionManager : MonoBehaviour
{
    public static ProgressionManager Instance { get; private set; }

    private GameMode gameMode = GameMode.Survival;
    public GameMode GameMode
    {
        get => gameMode;
        set => gameMode = value;
    }

    private bool isInitialized;
    [SerializeField] private string levelDirectory;
    public readonly List<string> levels = new List<string>();
    private void Awake()
    {
        Debug.Assert(Instance != this, "There are more than one progression manager in this scene"); 
        Instance = this;
        
        if(isInitialized) return;
        // find all the path to all in the levels directory 
        TextAsset[] files = Resources.LoadAll<TextAsset>(levelDirectory);
        foreach (TextAsset file in files)
        {
            if(Utils.IsMetaFile(file.name)) continue;
            levels.Add(levelDirectory + file.name);
        }

        isInitialized = true;

    }

    private int totalScore;
    public string CurrentLevelPath => levels[CurrentLevelIndex];
    public int CurrentLevelIndex { get; set; }
    public void LoadNextLevel(int scoreGainedThisLevel, LevelTransition transition)
    {
        totalScore += scoreGainedThisLevel;
        CurrentLevelIndex += 1;
        if (CurrentLevelIndex >= levels.Count)
            NoMoreLevels("Congratulations !");
        else
            transition.PlayAnimation(delegate { SceneManager.LoadScene("Scenes/SampleScene"); });
    }

    [SerializeField] private GameOverMenu gameOverMenu;
    private void NoMoreLevels(string message)
    { 
        GameOverMenu menu = Instantiate(gameOverMenu);
        menu.SetScore(totalScore);
        menu.SetMessage(message);
    }

    public void EndGame(int scoreGainThisLevel)
    {
        totalScore += scoreGainThisLevel;
        NoMoreLevels("You lost !");
    }
}

public enum GameMode
{
    Survival,
    Scoring
}