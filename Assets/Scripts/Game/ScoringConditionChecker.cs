﻿using System;

public class ScoringConditionChecker : VictoryConditionChecker
{
    private readonly int numberOfEnemiesToKill;
    private int numberOfEnemiesKilled;
    private int score;
    private readonly ComboParameters comboParameters;
    private int currentComboFactor;
    
    public override GameState PlayerGotHit(float remainingLife)
    {
        currentComboFactor = comboParameters.baseComboFactor;
        score -= comboParameters.penaltyOnHit;
        OnComboUpdatedEvent.Invoke(currentComboFactor);
        return GameState.Undecided;
    }

    public override GameState EnemyDied(int scoreWorthOnDeath)
    { 
        ++numberOfEnemiesKilled;
        score += scoreWorthOnDeath * currentComboFactor;
        if (currentComboFactor < comboParameters.maxComboFactor)
        {
            currentComboFactor += comboParameters.comboFactorIncrements;
            OnComboUpdatedEvent.Invoke(currentComboFactor);
        }

        if (numberOfEnemiesKilled >= numberOfEnemiesToKill)
            return GameState.Won;
        return GameState.Undecided;
    }

    public override int GetCurrentScore()
    {
        return score;
    }

    public ScoringConditionChecker(int numberOfEnemiesToKill, ComboParameters comboParameters)
    {
        this.numberOfEnemiesToKill = numberOfEnemiesToKill;
        this.comboParameters = comboParameters;
        currentComboFactor = comboParameters.baseComboFactor;

    }

    public void GainComboBonus(int multiplicator)
    {
        currentComboFactor *= multiplicator;
        OnComboUpdatedEvent.Invoke(currentComboFactor);
    }

    public delegate void OnComboUpdated(int comboBonus);
    public event OnComboUpdated OnComboUpdatedEvent;
}

[Serializable]
public struct ComboParameters
{
    public int baseComboFactor;
    public int comboFactorIncrements;
    public int maxComboFactor;
    public int penaltyOnHit;
}