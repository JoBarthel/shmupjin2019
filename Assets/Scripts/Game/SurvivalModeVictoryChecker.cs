﻿public class SurvivalModeVictoryChecker : VictoryConditionChecker
{
    public override GameState PlayerGotHit(float remainingLife)
    {
        if (remainingLife <= 0)
            return GameState.Lost;
        return GameState.Undecided;
    }

    private int score;
    private readonly int numberOfEnemiesToKill;
    private int numberOfEnemiesKilled;
    public override GameState EnemyDied(int scoreWorthOnDeath)
    {
        score += scoreWorthOnDeath;
        numberOfEnemiesKilled += 1;
        if (numberOfEnemiesKilled >= numberOfEnemiesToKill)
            return GameState.Won;
        return GameState.Undecided;
    }

    public override int GetCurrentScore()
    {
        return score;
    }

    public SurvivalModeVictoryChecker(int numberOfEnemiesToKill)
    {
        this.numberOfEnemiesToKill = numberOfEnemiesToKill;
    }
}