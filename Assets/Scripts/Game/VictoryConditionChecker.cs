﻿public abstract class VictoryConditionChecker
{
    public abstract GameState PlayerGotHit(float remainingLife);
    public abstract GameState EnemyDied(int scoreWorthOnDeath);
    public abstract int GetCurrentScore();
}

public enum GameState
{
    Won,
    Lost, 
    Undecided
}
