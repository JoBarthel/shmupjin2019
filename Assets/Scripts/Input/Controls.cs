﻿using UnityEngine;
using UnityEngine.InputSystem;

public class Controls
{
    private readonly InputMaster master;
    private readonly Engine engine;
    private readonly PlayerBulletGun gun;
    private bool isFiring;
    
    public Engine PlayerEngine => engine;
    public PlayerBulletGun PlayerGun => gun;

    public Controls(Engine engine, PlayerBulletGun gun)
    {
        this.engine = engine;
        this.gun = gun;
        master = new InputMaster();
        master.Enable();
        master.Player.Movement.performed += MoveEngine;
        master.Player.Movement.canceled += ctx => PlayerEngine.Stop();
        master.Player.Dodge.performed += ctx => PlayerEngine.Dodge();
        master.Player.Shoot.started += ctx => PlayerGun.StartFiring();
        master.Player.Shoot.canceled += ctx => PlayerGun.StopFiring();
        master.Player.SwitchWeapon.performed += ctx => PlayerGun.SwitchFire();
    }
    private void MoveEngine(InputAction.CallbackContext context)
    {
        Vector2 direction = context.ReadValue<Vector2>();
        PlayerEngine.Move(direction);
    }
}
