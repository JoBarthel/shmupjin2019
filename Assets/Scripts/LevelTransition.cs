﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelTransition : MonoBehaviour
{
    [SerializeField] private Text parallax1;
    [SerializeField] private Text parallax2;
    [SerializeField] private float animationTime;
    public void PlayAnimation(UnityAction callback)
    {
        StartCoroutine(Animate(callback));
    }

    private IEnumerator Animate(UnityAction callback)
    {
        float currentTime = 0f;
        float start = parallax1.transform.localPosition.x;
        while (currentTime < animationTime)
        {
            currentTime += Time.deltaTime;
            var fracTime = currentTime / animationTime;
            var newX = Mathf.Lerp(start, 0, fracTime);
            Vector2 temp = parallax1.transform.localPosition;
            temp.x = newX;
            parallax1.transform.localPosition = temp;
            yield return null;
        }
        
        callback.Invoke();
    }
    
}
