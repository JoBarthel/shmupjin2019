﻿using UnityEngine;

public class EnergyPickup : Pickup
{
    [SerializeField] public float bonus;
    public float Bonus => bonus;
}