﻿using UnityEngine;

public class HealthPickUp : Pickup
{
    [SerializeField] private int bonus;
    public int Bonus => bonus;
}