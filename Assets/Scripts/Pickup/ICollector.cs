﻿public interface ICollector
{
    bool Collect(Pickup pickup);
}