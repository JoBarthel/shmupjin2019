﻿using System;
using UnityEngine;
using UnityEngine.PlayerLoop;

public abstract class Pickup : MonoBehaviour, Pool<Pickup>.IPoolable
{
    [SerializeField] private PickupType type;
    public PickupType Type => type;

    private Pool<Pickup>.ReturnToPool returnToPool;

    [SerializeField] private float speed;
    private void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var collector = other.GetComponent<ICollector>();
        if (collector == null) return;
        if(collector.Collect(this));
            returnToPool(this);

    }

    public void SetReturnCallback(Pool<Pickup>.ReturnToPool callback)
    {
        returnToPool = callback;
    }
}