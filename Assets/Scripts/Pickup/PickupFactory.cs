﻿using System.Collections.Generic;
using UnityEngine;

public class PickupFactory : MonoBehaviour
{
    public static PickupFactory Instance { get; private set; }
    
    private static Dictionary<PickupType, Pool<Pickup>> pickupTypes;
    [SerializeField] private Pickup[] availablePickupPrefabs;

    private void Awake()
    {
        // Singleton boilerplate code
        Debug.Assert(Instance == null);
        Instance = this;
        
        // Build our dictionary of pickup types
        pickupTypes= new Dictionary<PickupType, Pool<Pickup>>();
        foreach (Pickup pickup in availablePickupPrefabs)
        {
            if (!pickupTypes.ContainsKey(pickup.Type))
                pickupTypes.Add(pickup.Type, new Pool<Pickup>(pickup, transform));
        }
    }

    /*
    * Return a pickup with the correct Type from a pool
    */
    public Pickup Get(PickupType lineBehaviour)
    {
        try
        {
            if (pickupTypes.ContainsKey(lineBehaviour)) return pickupTypes[lineBehaviour].Get();
        }
        catch(InstanceWasDestroyedException e)
        {
            RichLog.Warning(e.Message, typeof(PickupFactory));
        }

        throw new RequestUnknownTypeFromFactoryException();
    }
}
