﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class PickupSpawner
{
    private readonly Array pickupOptions;

    public PickupSpawner()
    {
        pickupOptions = Enum.GetValues(typeof(PickupType));
    }

    public void SpawnRandomPickup(Vector2 position)
    {
        var choice = pickupOptions.GetValue(Random.Range(0, pickupOptions.Length));
        var pickup = PickupFactory.Instance.Get( (PickupType) choice);
        pickup.transform.position = position;
    } 
}
