/**
 * Holds info related to a player that just died and the death's circumstances
 */
public struct DeathInfo
{
    public int scoreWorthOnDeath;
    public string whoDied;
}