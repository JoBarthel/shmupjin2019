using UnityEngine;

/**
 * Handle energy spending and regen
 */
public class EnergyDrive : MonoBehaviour
{
    public delegate void OnEnergyUpdated(float currentEnergy, float baseEnergy);
    public event OnEnergyUpdated OnEnergyUpdatedEvent;
    
    [SerializeField] private float baseEnergy;
    public float BaseEnergy
    {
        get => baseEnergy;
        set
        {
            baseEnergy = value;
            OnEnergyUpdatedEvent.Invoke(CurrentEnergy, BaseEnergy);
        }
    }
    
    private float currentEnergy;
    public float CurrentEnergy
    {
        get => currentEnergy;
        set
        {
            currentEnergy = value;
            OnEnergyUpdatedEvent.Invoke(CurrentEnergy, BaseEnergy);
        }
    }
    
    public bool IsAtMax => CurrentEnergy == BaseEnergy;

    private void Awake()
    {
        currentEnergy = baseEnergy;
    }
    
    public bool TrySpendEnergy(float quantity)
    {
        if (hasFailed) return false;
        SpendEnergy(quantity);
        return true;
    }

    private void SpendEnergy(float quantity)
    {
        CurrentEnergy -= quantity;
        if (CurrentEnergy <= 0) StartDriveFailure();
    }

    private bool hasFailed;
    public delegate void OnFailure();
    public event OnFailure OnFailureEvent;
    private void StartDriveFailure()
    {
        CurrentEnergy = 0;
        hasFailed = true;
        OnFailureEvent.Invoke();
    }
    
    private void Update()
    {
        RegenEnergy();
    }
    
    [SerializeField] private float energyRegenPerSecond;
    private void RegenEnergy()
    {
        if (!hasFailed)
        {
            CurrentEnergy += energyRegenPerSecond * Time.deltaTime;
            if (CurrentEnergy > BaseEnergy) CurrentEnergy = BaseEnergy;
            return;
        }

        CurrentEnergy += energyRegenPerSecond * .75f * Time.deltaTime;
        if (CurrentEnergy >= BaseEnergy)
            Recover();
        
    }

    public delegate void OnRecover();
    public event OnRecover OnRecoverEvent;
    private void Recover()
    {
        CurrentEnergy = BaseEnergy;
        hasFailed = false;
        OnRecoverEvent.Invoke();
    }
}