using System;

[Serializable]
public struct FireLine
{
    public float angle;
    public BulletBehaviour behaviour;
}