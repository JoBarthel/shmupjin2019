using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "FireMode")]
public class FireMode : ScriptableObject
{
    [SerializeField] public FireLine[] modes;
    [SerializeField] public float cost;
    [SerializeField] public float cooldown;
}
