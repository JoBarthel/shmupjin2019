﻿using System;
using UnityEngine;

public class PickupCollector : MonoBehaviour, ICollector
{
    private EnergyDrive energyDrive;
    private PlayerAvatar player;
    private PlayerBulletGun gun;
    private GameMode mode;
    
    private void Awake()
    {
        energyDrive = GetComponent<EnergyDrive>();
        player = GetComponent<PlayerAvatar>();
        gun = GetComponent<PlayerBulletGun>();
        
    }

    public bool Collect(Pickup pickup)
    {
        switch (pickup)
        {
            case HealthPickUp healthPickUp:
                return DoForHealthPick(healthPickUp);
            case EnergyPickup energyPickup:
                return DoForEnergyPickup(energyPickup);
            case WeaponPickup weaponPickup:
                return DoForWeaponPickup(weaponPickup);
            default:
                return false;
        }
    }

    private bool DoForWeaponPickup(WeaponPickup weaponPickup)
    {
        if(gun == null) return false;
        gun.ProgressUnlock();
        return true;
    }

    private bool DoForEnergyPickup(EnergyPickup energyPickup)
    {
        if(energyDrive == null) return false;
        if (energyDrive.IsAtMax) energyDrive.BaseEnergy += energyPickup.bonus;
        else energyDrive.CurrentEnergy += energyPickup.bonus;
        return true;
    }

    private bool DoForHealthPick(HealthPickUp healthPickUp)
    {
        if(player == null) return false;
        if (mode == GameMode.Scoring)
            GameManager.Instance.GainComboBonus(healthPickUp.Bonus);
        else
            player.Health += healthPickUp.Bonus;
        return true;
    }
}