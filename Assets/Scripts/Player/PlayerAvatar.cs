using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerAvatar : BaseAvatar
{
    /* We need the camera bounds in world space to limit the player to the play area */
    private Vector2 objectHalfSize;
    
    /* The player's movement zone is limited to this fractin of the screen width*/
    [SerializeField] private float playerLimit;
    private Rect cameraRect;

    protected override void Awake()
    {
        base.Awake();
        energyDrive = GetComponent<EnergyDrive>();
        
        // calculate the camera bounds in world space
        Bounds bounds = GetComponent<BoxCollider2D>().bounds;
        objectHalfSize = new Vector2(bounds.size.x / 2f, bounds.size.y / 2f);
        Camera main = Camera.main;
        Vector3 bottomLeft = main.ScreenToWorldPoint(Vector3.zero);
        Vector3 topRight = main.ScreenToWorldPoint(new Vector3(
            main.pixelWidth, main.pixelHeight));
 
        cameraRect = new Rect(
            bottomLeft.x,
            bottomLeft.y,
            topRight.x - bottomLeft.x,
            topRight.y - bottomLeft.y);

        playerAllowedArea = cameraRect.xMax / playerLimit;
    }

    private float playerAllowedArea;
    private void LateUpdate()
    {
        // after all movement is done, limit the player to his region of the screen
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, cameraRect.xMin + objectHalfSize.x,  cameraRect.xMin + playerAllowedArea - objectHalfSize.x);
        pos.y = Mathf.Clamp(pos.y, cameraRect.yMin + objectHalfSize.y, cameraRect.yMax - objectHalfSize.y);
        transform.position = pos;
    }

    private EnergyDrive energyDrive;
    public float Energy {
        get
        {
            if (energyDrive == null)
                return 0;
            return energyDrive.CurrentEnergy;
        }
    }


    public delegate void OnPlayerGotHit(float remainingLife);
    public event OnPlayerGotHit OnPlayerGotHitEvent;
    public override void SufferDamage(float damage)
    {
        if(isInvulnerable) return;
        Health -= damage;
        OnPlayerGotHitEvent.Invoke(health);
    }
    
    public delegate void OnHealthUpdated(float currentLife, float maxLife);
    public event OnHealthUpdated OnHealthUpdatedEvent;

    public float BaseHealth
    {
        get => baseHealth;
        set
        {
            baseHealth = value;
            OnHealthUpdatedEvent?.Invoke(health, baseHealth);
        }
    }
    public float Health
    {
        get => health;
        set
        {
            health = value;
            OnHealthUpdatedEvent?.Invoke(health, baseHealth);
        }
            
    }
    
}