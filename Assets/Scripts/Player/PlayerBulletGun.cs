﻿using UnityEngine;

public class PlayerBulletGun : BulletGun
{
    private EnergyDrive energyDrive;
    private float cost;

    protected override void Awake()
    {
        base.Awake();
        energyDrive = GetComponent<EnergyDrive>();
        if(!energyDrive) Debug.LogWarning($"Could not find energy drive in {GetType()} {name} ");
        cost = availableFireModes[currentFireMode].cost;
    }

    public override void Fire()
    {
        if(timeSpentOnCooldown < cooldown) return;
        timeSpentOnCooldown = 0;
        if(energyDrive.TrySpendEnergy(cost))
            base.Fire();
    }
    
    public void SwitchFire()
    {
        currentFireMode += 1;
        if (currentFireMode >= availableFireModes.Count)
            currentFireMode = 0;
        var mode = availableFireModes[currentFireMode];
        cost = mode.cost;
        cooldown = mode.cooldown;
    }

    [SerializeField] private int numberOfUnlockNeeded;
    public int NumberOfUnlockNeeded => numberOfUnlockNeeded;
    public int CurrentNumberOfUnlocks { get; private set; }
    private bool extraFireModeUnlocked;
    public void ProgressUnlock()
    {
        if(extraFireModeUnlocked) return;
        CurrentNumberOfUnlocks = CurrentNumberOfUnlocks + 1;
        if (CurrentNumberOfUnlocks >= NumberOfUnlockNeeded)
            UnlockFireMode();
        OnUnlockObtainedEvent.Invoke(CurrentNumberOfUnlocks, NumberOfUnlockNeeded);
    }

    public delegate void OnUnlockObtained( int obtained,int needed);
    public event OnUnlockObtained OnUnlockObtainedEvent;

    [SerializeField] private FireMode fireModeToUnlock;
    private void UnlockFireMode()
    {
        availableFireModes.Add(fireModeToUnlock);
        extraFireModeUnlocked = true;
    }
}
