using UnityEngine;

public struct EnemyInfo
{
    public EnemyType type;
    public float spawnTime;
    public Vector2 position;
}