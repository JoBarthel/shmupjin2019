using System.IO;
using UnityEngine;

/**
 * Load a level from the /Data/Levels folder
 */
public class LevelLoader : ILevelLoader
{
    public Level Load(string path)
    {
        var asset = Resources.Load<TextAsset>(path);
        var stream = new MemoryStream(asset.bytes);
        return LevelSerializer.Deserialize(stream);
    }
}

public interface ILevelLoader
{
    Level Load(string path);
}