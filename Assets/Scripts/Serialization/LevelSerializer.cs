using System;
using System.IO;
using System.Xml.Serialization;

public class LevelSerializer
{
    public static void Serialize(Level level, string filename)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Level));
        TextWriter writer = new StreamWriter(filename);
        serializer.Serialize(writer, level);
        writer.Close();
    }


    public static Level Deserialize(Stream stream)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Level));
        
        // handle errors
        serializer.UnknownNode += new XmlNodeEventHandler(SerializerUnknownNode);
        serializer.UnknownAttribute += new XmlAttributeEventHandler(SerializerUnknownAttribute);
        
        // read file
        Level level = (Level) serializer.Deserialize(stream);
        return level;
    }

    private static void SerializerUnknownNode(object sender, XmlNodeEventArgs e)
    {
        Console.WriteLine("Unknown Node : " + e.Name + "\t" + e.Text);
    }

    private static void SerializerUnknownAttribute(object sender, XmlAttributeEventArgs e)
    {
        System.Xml.XmlAttribute attr = e.Attr;
        Console.WriteLine("Unknown attribute " + attr.Name + "='" + attr.Value + "'");
    }
}