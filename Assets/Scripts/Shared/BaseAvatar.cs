﻿using System;
using System.Collections;
using UnityEngine;

public abstract class BaseAvatar : MonoBehaviour, IDamageable
{
    [SerializeField] private float maxSpeed;
    protected bool isInvulnerable;
    private Bullet bulletPrefab;
    private FlickerAnimation flickerer;

    protected float health;

    [SerializeField] protected float baseHealth;
    
    public float MaxSpeed
    {
        get => maxSpeed;
    }
    
    protected virtual void Awake()
    {
        flickerer = GetComponent<FlickerAnimation>();
        health = baseHealth;
    }
    public void StartInvulnerability(float invulnerabilityTimeInSeconds)
    {
        StartCoroutine( InvulnerabilityTimer(invulnerabilityTimeInSeconds));
        if(flickerer) flickerer.StartFlicker(invulnerabilityTimeInSeconds);
    }

   // todo: add animation
    IEnumerator InvulnerabilityTimer(float duration)
    {
        isInvulnerable = true;
        float spentDuration = 0;
        while (spentDuration <= duration)
        {
            spentDuration += Time.deltaTime;
            yield return null;
        }

        isInvulnerable = false;
    }

    public abstract void SufferDamage(float damage);

}
