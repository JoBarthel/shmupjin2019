﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class BulletGun : MonoBehaviour
{
    [SerializeField] protected float damage;
    [SerializeField] protected Vector2 speed;
    protected float cooldown;
    [SerializeField] protected BulletFaction bulletFaction;
    [SerializeField] protected List<FireMode> availableFireModes;

    protected int currentFireMode = 0;
    protected float timeSpentOnCooldown;
    private Vector2 Speed => speed;
    public float Cooldown
    {
        get => cooldown;
        set => cooldown = value;
    }

    protected virtual void Awake()
    {
        cooldown = availableFireModes[currentFireMode].cooldown;
    }

    private bool isFiring;
    
    public virtual void Fire()
    {
        var mode = availableFireModes[currentFireMode];
        foreach (var line in mode.modes)
        {
            GetBullet(line);
        }
    }

    private void GetBullet(FireLine line)
    {
        var bullet = BulletFactory.Instance.Get(line.behaviour);
        var startingDirection =  Quaternion.Euler(0, 0, line.angle) * transform.right;
        bullet.Init(Speed, damage, bulletFaction, startingDirection, transform.position);
        bullet.gameObject.layer = LayerMask.NameToLayer("Projectile");
    }

    private void Update()
    {
        if(timeSpentOnCooldown < cooldown)
            timeSpentOnCooldown += Time.deltaTime;
        
        if(isFiring) Fire();
    }

    public void StartFiring()
    {
        isFiring = true;
    }
    
    public void StopFiring()
    {
        isFiring = false;
    }

}
