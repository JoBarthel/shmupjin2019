using UnityEngine;

public class Engine : MonoBehaviour
{
    [SerializeField] private float dodgeLengthInPixels;
    [SerializeField] private float invulnerabilityTimeInSeconds;
    [SerializeField] private float dodgeCost;
    
    private Vector2 velocity;
    private float speed;
    private BaseAvatar avatar;
    
    /* we need the scene's camera to measure distance in pixels*/
    private new Camera camera;
    private Vector2 currentFacingDirection;
    private EnergyDrive energyDrive;
    public float Speed
    {
        get => speed;
        set => speed = value;
    }

    private void Awake()
    {
        avatar = GetComponent<BaseAvatar>();
        camera = Camera.main;
        energyDrive = GetComponent<EnergyDrive>();
    }

    private void Update()
    {
        currentFacingDirection = velocity.normalized;
        transform.Translate(velocity  * Time.deltaTime);
    }

    public void Move(Vector2 direction)
    {
        velocity = direction * Mathf.Max(avatar.MaxSpeed, Speed);
    }

    public void Dodge()
    {
        if (!energyDrive || !energyDrive.TrySpendEnergy(dodgeCost)) return;
        avatar.StartInvulnerability(invulnerabilityTimeInSeconds);
        
        // some calculations to get the distance in pixels
        var position = transform.position;
        Vector2 startingPointInScreenSpace = camera.WorldToScreenPoint(position);
        var arrivalPointInScreenSpace = startingPointInScreenSpace + currentFacingDirection * dodgeLengthInPixels;
        var newPos = camera.ScreenToWorldPoint(arrivalPointInScreenSpace);
        
        // set Z coordinate or our player will end up at the camera's Z 
        newPos.z = position.z;
        transform.position = newPos;
    }

    public void Stop()
    {
        velocity = Vector2.zero;
    }
}