﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ComboDisplay : MonoBehaviour
{
    private ScoringConditionChecker scoringConditionChecker;
    private Text text;
    [SerializeField] private string prefix;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    public void Init(ScoringConditionChecker scoringConditionChecker)
    {
        scoringConditionChecker.OnComboUpdatedEvent += UpdateText;
    }
    
    public void UpdateText(int comboValue)
    {
        text.text = $"{prefix}{comboValue.ToString()}";
    }
}
