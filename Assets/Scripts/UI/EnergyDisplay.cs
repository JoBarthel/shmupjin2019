﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class EnergyDisplay : MonoBehaviour
{
    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    public void Init(EnergyDrive energyDrive)
    {
        UpdateText(energyDrive.CurrentEnergy, energyDrive.BaseEnergy);
        energyDrive.OnEnergyUpdatedEvent += UpdateText;
    }

    private void UpdateText(float energyDriveCurrentEnergy, float energyDriveBaseEnergy)
    {
        text.text = $"{ Mathf.Round(energyDriveCurrentEnergy).ToString()}/{ Mathf.Round(energyDriveBaseEnergy).ToString()}";
    }
}
