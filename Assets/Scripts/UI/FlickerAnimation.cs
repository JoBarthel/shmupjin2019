﻿using System.Collections;
using UnityEngine;

public class FlickerAnimation : MonoBehaviour
{
    [SerializeField] private SpriteRenderer flickerTarget;
    [SerializeField] private float animationSpeed;
    private Coroutine coroutine;

    public void StartFlicker(float duration)
    {
        coroutine = StartCoroutine(Flicker(duration));
    }

    public void StopFlicker()
    {
        StopCoroutine(coroutine);
    }
    
    private IEnumerator Flicker(float timer)
    {
        var tempColor = flickerTarget.color;
        float timeSpent = 0;
        while (timeSpent < timer)
        {
            timeSpent += Time.deltaTime;
            tempColor.a = Mathf.Cos(timeSpent * animationSpeed);
            flickerTarget.color = tempColor;
            yield return null;
        }

        tempColor.a = 1;
        flickerTarget.color = tempColor;
    }
}
