﻿using UnityEngine;
using UnityEngine.UI;

public class GameModeSelect : MonoBehaviour
{
    [SerializeField] private Button survivalButton;
    [SerializeField] private Button scoringButton;
    [SerializeField] private GameObject levelSelectPanel;

    private void Start()
    {
        survivalButton.onClick.AddListener(delegate
        {
            ProgressionManager.Instance.GameMode = GameMode.Survival;
            levelSelectPanel.SetActive(true);
            gameObject.SetActive(false);
        });
        scoringButton.onClick.AddListener(delegate
        {
            ProgressionManager.Instance.GameMode = GameMode.Scoring;
            levelSelectPanel.SetActive(true);
            gameObject.SetActive(false);
        });
    }
}
