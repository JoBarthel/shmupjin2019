﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] private string prefix;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text messageText;
    public void SetScore(float totalScore)
    {
        
        scoreText.text =$"{prefix}{totalScore.ToString()}";
    }

    public void SetMessage(string message)
    {
        messageText.text = message;
    }
}
