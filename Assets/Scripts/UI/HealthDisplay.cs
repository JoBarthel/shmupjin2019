﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HealthDisplay : MonoBehaviour
{
   private Text text;

   private void Awake()
   {
      text = GetComponent<Text>();
   }

   public void Init(PlayerAvatar player)
   {
      UpdateText(player.Health, player.BaseHealth);
      player.OnHealthUpdatedEvent += UpdateText;
   }

   private void UpdateText(float currentHealth, float baseHealth)
   {
      text.text = $"{currentHealth.ToString()}/{baseHealth.ToString()}";
   }
}
