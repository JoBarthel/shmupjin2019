﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

/**
 * Instantiate a buttonPrefab per level in the game as child of the LayoutGroup group
 */
public class LevelSelect : MonoBehaviour
{
    
    [SerializeField] private LevelSelectionButton buttonPrefab;
    [SerializeField] private LayoutGroup group;
    private void Start()
    {
        for (int index = 0; index < ProgressionManager.Instance.levels.Count; index++)
        {
            var level = ProgressionManager.Instance.levels[index];
            LevelSelectionButton button = Instantiate(buttonPrefab, group.transform);
            button.LevelIndex = index;
            button.SetText(  Path.GetFileNameWithoutExtension(level) );
        }
    }
}
