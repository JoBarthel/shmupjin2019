﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

/**
 * A button that will load the level of index LevelIndex on click
 */
public class LevelSelectionButton : Button
{
    public int LevelIndex { get; set; }
    private Text label;
    protected override void Start()
    {
        base.Start();
        onClick.AddListener(LoadLevel);
    }

    private void LoadLevel()
    {
        ProgressionManager.Instance.CurrentLevelIndex = LevelIndex;
        SceneManager.LoadScene("Scenes/SampleScene");
    }

    public void SetText(string text)
    {
        label = GetComponentInChildren<Text>();
        if(label)
            label.text = text;
    } 
}
