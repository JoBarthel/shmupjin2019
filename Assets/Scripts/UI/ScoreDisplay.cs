﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreDisplay : MonoBehaviour
{
    [SerializeField] private string prefix;
    [SerializeField] private GameManager scoreProvider;
    private Text text;
    private void Start()
    {
        text = GetComponent<Text>();
        if (scoreProvider == null)
        {
            RichLog.Warning("No Score Provider in Score Display, did you forget to set it in the editor ?", this);
            return;
        }

        scoreProvider.OnScoreChangedEvent += DisplayScore;
    }

    private void DisplayScore(int score)
    {
        text.text = $"{prefix}{score.ToString()}";
    }
}
