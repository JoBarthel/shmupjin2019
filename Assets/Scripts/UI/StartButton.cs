﻿using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Start the game on click
 */
public class StartButton : MonoBehaviour
{
    [SerializeField] private string firstLevel;

    public void LoadFirstLevel()
    {
        SceneManager.LoadScene(firstLevel);
        
    }
}
