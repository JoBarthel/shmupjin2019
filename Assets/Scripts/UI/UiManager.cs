﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/**
 * Handle player status display
 */
public class UiManager : MonoBehaviour
{
    private PlayerAvatar player;
    [SerializeField] private WeaponUnlockDisplay weaponUnlockDisplay;
    [SerializeField] private HealthDisplay healthDisplay;
    [SerializeField] private EnergyDisplay energyDisplay;
    [SerializeField] private ComboDisplay comboDisplay;
    public void Init(PlayerAvatar player, VictoryConditionChecker checker)
    {
        this.player = player;
        var energyDrive = player.GetComponent<EnergyDrive>();
        Debug.Assert(energyDrive != null, "Player has no energy drive !");
        energyDrive.OnFailureEvent += StartFailureAnimation;
        energyDrive.OnRecoverEvent += StopFailureAnimation;
        energyDisplay.Init(energyDrive);

        var gun = player.GetComponent<PlayerBulletGun>();
        Debug.Assert(gun != null, "Player has no gun !");
        weaponUnlockDisplay.Init(gun);
        if (checker is ScoringConditionChecker scoringConditionChecker)
        {
            comboDisplay.gameObject.SetActive(true);
            comboDisplay.Init(scoringConditionChecker);
        }
        else if(checker is SurvivalModeVictoryChecker)
        {
            healthSlider.gameObject.SetActive(true);
            healthDisplay.Init(player);
        }
    }
    
    [SerializeField] private Slider healthSlider;
    [SerializeField] private Slider energySlider;
    private void Update()
    {
        healthSlider.value = player.Health;
        energySlider.value = player.Energy;
    }

    private Coroutine currentFailureAnimation;
    private void StartFailureAnimation()
    {
        currentFailureAnimation = StartCoroutine(FailureAnimation());
    }

    private void StopFailureAnimation()
    {
        StopCoroutine(currentFailureAnimation);
        Utils.SetColor(ref failureText, 0);
    }

    [SerializeField] private Graphic failureText;
    [SerializeField] private float animationSpeed;
    private IEnumerator FailureAnimation()
    {
        Color tempColor = failureText.color;
        while (true)
        {
            tempColor.a = Mathf.Cos(Time.time * animationSpeed);
            failureText.color = tempColor;
            yield return null;
        }
    }
}