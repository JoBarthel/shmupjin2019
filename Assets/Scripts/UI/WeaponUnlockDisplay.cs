﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class WeaponUnlockDisplay : MonoBehaviour
{
    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    public void Init(PlayerBulletGun gun)
    {
        UpdateText( gun.CurrentNumberOfUnlocks, gun.NumberOfUnlockNeeded);
        gun.OnUnlockObtainedEvent += UpdateText;
    }
    
    void UpdateText(int unlockObtained,int unlockNeeded)
    {
        text.text = $"{unlockObtained.ToString()}/{unlockNeeded.ToString()}";
    }
}
