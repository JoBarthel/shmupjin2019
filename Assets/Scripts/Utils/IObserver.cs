public interface IObserver<T>
{
    void Notify(T info);
}