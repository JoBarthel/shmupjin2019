﻿using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class Pool<T> where T : MonoBehaviour, Pool<T>.IPoolable
{
    private readonly T prefab;
    private readonly Transform parent;
    
    private readonly Queue<T> elements = new Queue<T>();

    public Pool(T prefab, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }
    
    public T Get()
    {
        if (elements.Count == 0)
        {
            AddElement();
        }
        
        var ret = elements.Dequeue();
        ret.transform.SetParent(parent);
        ret.gameObject.SetActive(true);

        return ret;
    }

    private void AddElement()
    {
        T element = Object.Instantiate(prefab);
        element.SetReturnCallback(Return);
        elements.Enqueue(element);
    }

    private void Return(T element)
    {
        element.gameObject.SetActive(false);
        elements.Enqueue(element);
    }

    public delegate void ReturnToPool(T element);

    public interface IPoolable
    {
        void SetReturnCallback(ReturnToPool callback);
    }
}