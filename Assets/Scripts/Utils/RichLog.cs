using System;
using UnityEngine;

public class RichLog
{
    const int size = 14;
    public static void Log(String message, object context)
    {
        Debug.Log($"<size={size}> {context.GetType()} : {message} </size>");
    }
    
    public static void Warning(String message, object context)
    {
        Debug.LogWarning($"<size={size}> {context.GetType()} : {message} </size>");
    }
}