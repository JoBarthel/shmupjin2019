﻿using UnityEngine;
using UnityEngine.UI;

public static class Utils
{
    public static bool IsMetaFile(string file)
    {
        string[] words = file.Split('.');
        return words[words.Length - 1] == "meta";
    }

    public static void SetColor(ref Graphic graphic, float alpha)
    {
        Color tempColor = graphic.color;
        tempColor.a = alpha;
        graphic.color = tempColor;
    }
}