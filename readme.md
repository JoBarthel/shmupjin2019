﻿# Shmup JIN 2019

## How to play

Start the project in Unity3D. Select the "Menu" scene. Press Play. 

You can also play the game in browser at : https://spawnie.itch.io/shmup-jin-2019
### Controls 

- ZQSD or arrows keys to move
- Space to fire
- Tab to switch fire
- Double tap a key to dodge
- Cogs are health pick-ups
- Energy cells are energy pick-ups
- If you collect 3 missiles, you will unlock an extra fire mode
- There's a little bit of time at the end of the level before the next one starts loading
- 
## Features

- 2 Enemy Types
- 3 Pickup Types
- 3 Modes of fire
- 3 levels
- 2 modes of play

## How to add levels ? 

Add a .xml file in the Resources/Levels subfolder. Some examples are provided.

## Controller support 

This game supports controller input. Unfortunately, my controller broke down a couple days ago so I wasn't able to test the final release controller support. 
